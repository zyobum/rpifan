#!/usr/bin/env python
# encoding: utf-8
# From:shumeipai.net
import RPi.GPIO
import time

start = 49
stop = 42
RPi.GPIO.setwarnings(False)
RPi.GPIO.setmode(RPi.GPIO.BCM)
RPi.GPIO.setup(2, RPi.GPIO.OUT)
pwm = RPi.GPIO.PWM(2,20)
fan = False
pwm.stop()
print("Fan control start.")
try:
    while True:
        with open('/sys/class/thermal/thermal_zone0/temp') as f:
            cur = int(f.read()) / 1000
            now = time.strftime("%H:%M:%S",time.localtime(time.time()))
            print("[%s] Temp: %s" % (now, cur))
            if cur > start:
                fan = True
            if cur < stop:
                fan = False
            if fan:
                p=min(100,int((cur-stop)*0.8)*5+60)
                pwm.start(p)
                print("[%s] Fan on @ %s (%s)" % (now, cur, p))
            else:
                pwm.stop()
                print("[%s] Fan off @ %s" % (now, cur))

            time.sleep(3)
except KeyboardInterrupt:
    pwm.stop()